module "dev_cluster" {
  source        = "./cluster"
  cluster_name  = "dev-int-alpha"
  instance_type = "t2.micro"
}